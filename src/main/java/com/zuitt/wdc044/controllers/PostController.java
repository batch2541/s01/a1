package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PostController {
    @Autowired
    private PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllPosts() {
        Iterable<Post> posts = postService.getPosts();
        for (Post post : posts) {
            post.getUser().setPassword("Confidential");
        }
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }
}

